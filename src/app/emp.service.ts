import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  
  getDepartmentById() {
    throw new Error('Method not implemented.');
  }

  isUserLoggedIn: boolean;
  cartItems: any;
  loginStatus: any;
  

  constructor(private http:HttpClient) { 
    this.isUserLoggedIn = false;
    //Cart using Service
    this.cartItems = [];
    this.loginStatus = new Subject();
  }

  getAllCountries():any{
    return this.http.get('https://restcountries.com/v3.1/all') ;
  }
  
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }  
  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  getEmployees(): any {
    return this.http.get('http://localhost:8085/getEmployees');
  }
 
  
    //Cart using Service
    addToCart(product: any) {
      this.cartItems.push(product);
    }

    //Cart using Service
    getCartItems(): any {
      return this.cartItems;
    }

     //get employeeById
     getEmployeeById(empId: any): any {
      return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
    }

    //DeptId
    getAllDepartments(): any {
      return this.http.get('http://localhost:8085/getDepartments');
    }
    //Register
    regsiterEmployee(employee: any): any {
      return this.http.post('http://localhost:8085/addEmployee', employee);
    }

    //delete product from the cart
    removeFromCart(productId:any){
      this.cartItems = this.cartItems.filter((item:any) =>
        item.id !== productId);
    }

    //lOGING EMPLOYEE
    employeeLogin(emailId: any, password: any): any {
      return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
    }


    
    updateEmployee(employee: any) {
      return this.http.put('http://localhost:8085/updateEmployee', employee);
    }
    
    //delete employee 
    deleteEmployee(empId: any) {
      return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
    }
}