import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{

    id: number;
    Name: string;
    avg: number;
    address:any;
    hobby:any;

  constructor(){
    // alert("constructor invoked")
   this.id = 101;
   this.Name='vedavyasa';
   this.avg=45.5;
   this.address ={
    streetNo:251,
    city:'Hyderabad',
    state:'Telangana'
   }
   this.hobby = ['sleeping', 'Eating','Playing']
  }

  ngOnInit() {
    
  }

}
