import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exp'
})
export class ExpPipe implements PipeTransform {
  experience:any;
  currentYear:any;
  jionYear: any;

  transform(value:any): any {
    
    this.currentYear = new Date().getFullYear();
    this.jionYear  = new Date(value).getFullYear();
    this.experience = this.currentYear-this.jionYear;

    return this.experience;
  }

}
