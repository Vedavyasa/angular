import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit{
  products:any;
  cartProducts: any;
  
  constructor(private service: EmpService){
    this.cartProducts = [];
    this.products = [
      { 
        id:1,
        name: 'Earphones',
        image: 'assets/images/earphone.jpg',
        price:'500',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      },
      { 
        id:2,
        name: 'Laptop',
        image: 'assets/images/laptop.avif',
        price:'53,000',
        description: 'No cost Emi'
      },
      { 
        id:3,
        name: 'Mobile',
        image: 'assets/images/mobile.jpg',
        price:'21,000',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      },
      { 
        id:4,
        name: 'Refrigerator',
        image: 'assets/images/Refrigirator.webp',
        price:'25,000',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      }
    ];
  }
  ngOnInit(){
    
  }

  addToCart(product: any) {
    // this.cartProducts.push(product);
    // localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));

    //Cart using Service
    this.service.addToCart(product);
  }
}
