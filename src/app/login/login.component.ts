import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
//Import Router class
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
//Import EmpService


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emp: any;
  toastr: any;
  captcha: string = '';
  captchaInput: string = '';

  //Dependency Injection for EmpService, Router
  constructor(private router: Router, private service: EmpService) {   
  }

  ngOnInit():void{
    this.generateCaptcha();
  }

    // Validate captcha
    isValidCaptcha(captchaInput: string): boolean {
      return captchaInput === this.captcha;
    }
  
    // Generate random captcha
    generateCaptcha(): void {
      const possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      const length = 6;
      this.captcha = Array.from({ length }, () =>
        possibleChars[Math.floor(Math.random() * possibleChars.length)]
      ).join('');
    }

  async loginSubmit(loginForm: any) {
    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {           
      //Setting the isUserLoggedIn variable value to true under EmpService
      this.service.setIsUserLoggedIn();
      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['showemps']);
    } else {
      this.emp = null;

      await this.service.employeeLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data);
        this.emp = data;
      });


      if (this.emp != null) {
        //Setting the isUserLoggedIn variable value to true under EmpService
      this.service.setIsUserLoggedIn();

      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['showemps']);
      } else {

        this.toastr.error('Invalid Credentials', 'Error', {
          closeButton: true,
          progressBar: true,
          positionClass: 'toast-top-right',
          tapToDismiss: false,
          timeOut: 2000, // 2 seconds
        });
        return;
      }
    }
    this.toastr.success('Login Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 2000, // 2 seconds
    });
  }
}