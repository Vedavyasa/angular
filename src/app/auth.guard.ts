import { CanActivateFn } from '@angular/router';
import { EmpService } from './emp.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  let service = inject(EmpService);
  return service.getIsUserLogged();
};
